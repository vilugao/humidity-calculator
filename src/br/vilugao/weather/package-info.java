/**
 * Contains the core classes used for weather related calculations.
 *
 * @author Vin&iacute;cius Lug&atilde;o
 */
package br.vilugao.weather;
