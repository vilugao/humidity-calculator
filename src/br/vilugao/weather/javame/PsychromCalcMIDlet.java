package br.vilugao.weather.javame;

import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;

import br.vilugao.weather.Psychrometric;
import br.vilugao.weather.PsychrometricStatus;

public final class PsychromCalcMIDlet extends MIDlet
implements CommandListener {
    private static final Command EXIT_COMMAND
            = new Command("Exit", Command.EXIT, 0);
    private static final Command CALC_COMMAND
            = new Command("Calculate", Command.OK, 0);
    private static final Command BACK_COMMAND
            = new Command("Back", Command.BACK, 0);

    private Form inputForm;
    private TextField tempField;
    private ChoiceGroup psychromCG;
    private TextField psychromField;

    private final Psychrometric psychrometric;

    private Form resultForm;
    private StringItem tempSI;
    private StringItem dptSI;
    private StringItem rhSI;
    private StringItem hiSI;
    private StringItem ahSI;

    public PsychromCalcMIDlet() {
        psychrometric = new Psychrometric();
    }

    private Form getInputForm() {
        if (inputForm == null) {
            tempField = new TextField("Air temperature (\u00b0C):", null, 3,
                                      TextField.NUMERIC);
            psychromCG = new ChoiceGroup(null, Choice.POPUP,
                                         new String[] {
                                             "Dew point (\u00b0C):",
                                             "Relative humidity (%):",
                                             "Wet-bulb temperature (\u00b0C):"
                                         },
                                         null);
            psychromField = new TextField(null, null, 3, TextField.NUMERIC);

            inputForm = new Form(getAppProperty("MIDlet-Name"),
                    new Item[]{
                        tempField,
                        psychromCG,
                        psychromField,
                    });
            inputForm.addCommand(EXIT_COMMAND);
            inputForm.addCommand(CALC_COMMAND);
            inputForm.setCommandListener(this);
        }
        return inputForm;
    }

    private Form getResultForm() {
        if (resultForm == null) {
            tempSI = new StringItem("Air temperature:", "N/A.");
            dptSI = new StringItem("Dew point:", "N/A.");
            rhSI = new StringItem("Relative humidity:", "N/A.");
            hiSI = new StringItem("Heat index:", "N/A.");
            ahSI = new StringItem("Absolute humidity:", "N/A.");

            resultForm = new Form(getAppProperty("MIDlet-Name"),
                    new Item[]{
                        tempSI,
                        dptSI,
                        rhSI,
                        hiSI,
                        ahSI,
                    });
            resultForm.addCommand(BACK_COMMAND);
            resultForm.setCommandListener(this);
        }
        return resultForm;
    }

    public void startApp() {
        Display.getDisplay(this).setCurrent(getInputForm());
    }

    public void pauseApp() {}

    public void destroyApp(final boolean unconditional) {}

    public void commandAction(final Command c, final Displayable d) {
        if (c == EXIT_COMMAND) {
            destroyApp(true);
            notifyDestroyed();
        } else if (c == CALC_COMMAND) {
            final Form f = getResultForm();
            final Alert a = calculate();
            if (a == null) {
                Display.getDisplay(this).setCurrent(f);
            } else {
                Display.getDisplay(this).setCurrent(a, getInputForm());
            }
        } else if (c == BACK_COMMAND) {
            Display.getDisplay(this).setCurrent(getInputForm());
        }
    }

    private Alert calculate() {
        final int temp;
        int dpt;
        final double rh;

        if (tempField.size() < 1) {
            return getNoTemperatureAlert();
        }

        if (psychromField.size() < 1) {
            return getNoRHRelatedAlert();
        }

        temp = Byte.parseByte(tempField.getString());
        dpt = Byte.parseByte(psychromField.getString());

        switch (psychromCG.getSelectedIndex()) {
        case 0:
            /* Dew point selected */
            if (dpt > temp) {
                return getDewPointInvalidAlert();
            }
            rh = psychrometric.rhFromDewPoint(dpt, temp);
            break;

        case 1:
            /* RH selected */
            if (dpt <= 0 || dpt > 100) {
                return getRHInvalidAlert();
            }
            rh = dpt / 100.0;
            dpt = (int) (psychrometric.dewPointTemperature(rh, temp) + .5);
            break;

        case 2:
            /* Wet-bulb temperature selected */
            if (dpt > temp) {
                return getDewPointInvalidAlert();
            }
            rh = psychrometric.rhFromWBT(dpt, temp);
            dpt = (int) (psychrometric.dewPointTemperature(rh, temp) + .5);
            break;

        default:
            return null;
        }

        updateResult(temp, dpt, rh);
        return null;
    }

    private void updateResult(final int t, final int dpt, final double rh) {
        tempSI.setText(String.valueOf(t) + "\u00a0\u00b0C.");

        final int dpt_idx = PsychrometricStatus.getDewPointStatusIndex(dpt);
        dptSI.setText(String.valueOf(dpt)
                + "\u00a0\u00b0C ("
                + PsychrometricStatus.DEWPOINT_STATUS_STRINGS[dpt_idx]
                + ").");

        final int rh_int = (int) (100.0 * rh + .5);
        final int rh_idx = PsychrometricStatus.getRHStatusIndex(rh_int);
        rhSI.setText(String.valueOf(rh_int)
                + "\u00a0% ("
                + PsychrometricStatus.RH_STATUS_STRINGS[rh_idx]
                + ").");

        if (t >= 27 && rh >= 0.4) {
            final int hi = (int) (Psychrometric.heatIndex(t, rh) + .5);
            final int hi_idx = PsychrometricStatus.getHeatIndexStatusIndex(hi);
            hiSI.setText(String.valueOf(hi)
                    + "\u00a0\u00b0C ("
                    + PsychrometricStatus.HEATINDEX_STATUS_STRINGS[hi_idx]
                    + ").");
        } else {
            hiSI.setText("N/A.");
        }

        final int ah = (int) (psychrometric.absoluteHumidity(rh, t) + .5);
        ahSI.setText(String.valueOf(ah) + "\u00a0g/m\u00b3.");
    }

    private static Alert getNoTemperatureAlert() {
        return new Alert("Input required",
            "Please enter the air temperature in \u201cAir temperature\u201d field.",
            null, AlertType.ERROR);
    }

    private static Alert getNoRHRelatedAlert() {
        return new Alert("Input required",
            "Please enter value of Dew point temperature, Relative humidity, or Wet-bulb temperature.",
            null, AlertType.ERROR);
    }

    private static Alert getDewPointInvalidAlert() {
        return new Alert("Invalid input",
            "Please enter the Dew point temperature value less than or equal to Air temperature.",
            null, AlertType.ERROR);
    }

    private static Alert getRHInvalidAlert() {
        return new Alert("Invalid input",
            "Please enter the Relative humidity value between 1 to 100.",
            null, AlertType.ERROR);
    }
}
