package br.vilugao.weather;

import br.vilugao.javame.util.Math;

/**
 * The <code>Psychrometric</code> class contains constants and methods
 * for performing psychrometric calculations.
 * <p>
 * References:
 * <ul>
 * <li>Wikipedia:
 *     <a href="https://en.wikipedia.org/wiki/Psychrometrics">Psychrometrics</a>.</li>
 * <li>Wikipedia:
 *     <a href="https://en.wikipedia.org/wiki/Dew_point">Dew point</a>.</li>
 * <li>NOAA; tim.brice.
 *     <i><a href="http://www.srh.noaa.gov/images/epz/wxcalc/rhTdFromWetBulb.pdf">
 *     Relative Humidity and Dewpoint Temperature from Temperature and
 *     Wet-Bulb Temperature</a></i>.</li>
 * <li>Paroscientific, Inc. Precision Pressure Instrumentation.
 *     <i><a href="http://www.paroscientific.com/dewpoint.htm">
 *     MET4 and MET4A calculation of dew point</a></i>.</li>
 * <li>Wikipedia:
 *     <a href="https://en.wikipedia.org/wiki/Heat_index">Heat index</a>.</li>
 * <li>NOAA; tim.brice.
 *     <i><a href="http://www.srh.noaa.gov/images/epz/wxcalc/heatIndex.pdf">
 *     Heat Index</a></i>.</li>
 * </ul>
 *
 * @author Vin&iacute;cius Lug&atilde;o
 */
public final class Psychrometric {
    /**
     * The constant <i>a</i>.
     *
     * @see #Psychrometric(double, double, double)
     */
    private final double a;
    /**
     * The constant <i>b</i>.
     *
     * @see #Psychrometric(double, double, double)
     */
    private final double b;
    /**
     * The constant <i>c</i> in degree Celsius.
     *
     * @see #Psychrometric(double, double, double)
     */
    private final double c;

    /**
     * Initializes a newly created <code>Psychrometric</code>
     * calculation object using Sonntag1990 constants.
     * The constants used by Sonntag1990:
     * <ul>
     * <li><i>a</i> = 6.112 hPa;</li>
     * <li><i>b</i> = 17.62;</li>
     * <li><i>c</i> = 243.12 &deg;C.</li>
     * </ul>
     *
     * @see #Psychrometric(double, double, double)
     */
    public Psychrometric() {
        this.a = 6.112;
        this.b = 17.62;
        this.c = 243.12;
    }

    /**
     * Initializes a newly created <code>Psychrometric</code>
     * calculation object using the constants <i>a</i>, <i>b</i> and
     * <i>c</i> given from arguments.
     *
     * @param a  the constant <i>a</i> in hectopascal (hPa).
     * @param b  the constant <i>b</i>.
     * @param c  the constant <i>c</i> in degree Celsius.
     * @see #Psychrometric()
     * @see #vaporPressure(double)
     */
    public Psychrometric(final double a, final double b, final double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private double gamma(final double t) {
        return b * t / (t + c);
    }

    /**
     * Returns the saturated vapor pressure at given temperature.
     * The formula used here is: {@code vp(t) = A * exp(B * t / (t + C))}.
     *
     * @param t  the temperature in degree Celsius.
     * @return the vapor pressure in millibar (mbar) or hectopascal (hPa).
     * @see #rhFromDewPoint(double, double)
     * @see #rhFromWBT(double, double)
     * @see #absoluteHumidity(double, double)
     */
    public double vaporPressure(final double t) {
        return a * Math.exp(gamma(t));
    }

    /**
     * Returns the relative humidity from the air temperature and
     * the dew point temperature.
     * The relative humidity is a ratio between vapor saturation
     * pressure and actual vapor pressure.
     * The formula used here is:
     * {@code rh(dpt, t) = vaporPressure(dpt) / vaporPressure(t) }.
     *
     * @param dpt  the dew point temperature in degree Celsius.
     * @param t  the air temperature in degree Celsius.
     * @return the relative humidity (%) in range of <code>0.0</code>
     *         to <code>1.0</code>.
     * @see #rhFromWBT(double, double)
     * @see #vaporPressure(double)
     */
    public double rhFromDewPoint(final double dpt, final double t) {
        if (dpt >= t) {
            return 1.0;
        }
        return Math.exp(gamma(dpt) - gamma(t));
    }

    /**
     * Returns the relative humidity from the air temperature and the wet-bulb
     * temperature.
     * The formula used here is:
     * {@code rh(wbt, t) = (vaporPressure(wbt) + offset) / vaporPressure(t) },
     * where {@code offset = -66E-5 * p_sta * (t - wbt) * (1.15E-3 * wbt + 1) },
     * and assuming the station pressure (p_sta) is 1000 hPa.
     *
     * @param wbt  the wet-bulb temperature in degree Celsius.
     * @param t  the air temperature in degree Celsius.
     * @return the relative humidity (%) in range of <code>0.0</code>
     *         to <code>1.0</code>.
     * @see #rhFromDewPoint(double, double)
     */
    public double rhFromWBT(final double wbt, final double t) {
        final double off;

        if (wbt >= t) {
            return 1.0;
        }
        off = -0.66 * (t - wbt) * (1.15E-3 * wbt + 1);
        return (vaporPressure(wbt) + off) / vaporPressure(t);
    }

    /**
     * Returns the absolute humidity.
     * The formula used here is:
     * {@code AH = e / (R_v * T) }, where:
     * <ul>
     * <li>{@code e = vaporPressure(t) * rh}, this
     * is the actual vapor pressure in Pascal;</li>
     * <li>{@code T} is the temperature in Kelvin;</li>
     * <li>{@code R_v} is the specific gas constant for water vapor,
     * the value is 0.4615 J/(g&middot;K).</li>
     * </ul>
     *
     * @param rh  the relative humidity (%) in range of <code>0.0</code>
     *            to <code>1.0</code>.
     * @param t  the air temperature in degree Celsius.
     * @return the absolute humidity in grams per cubic metre (g/m&sup3;).
     * @see #rhFromDewPoint(double, double)
     * @see #vaporPressure(double)
     */
    public double absoluteHumidity(final double rh, final double t) {
        return vaporPressure(t) * rh / (4.615e-3 * (t + 273.15));
    }

    /**
     * Returns the dew point temperature.
     * The formula used here is:
     * {@code dpt = C * g / (B - g) }, where
     * {@code g = ln(rh) + B * t / (t + C) }.
     *
     * @param rh  the relative humidity (%) in range of <code>0.0</code>
     *            to <code>1.0</code>.
     * @param t  the air temperature in degree Celsius.
     * @return the dew point temperature in degree Celsius.
     * @see #rhFromDewPoint(double, double)
     */
    public double dewPointTemperature(final double rh, final double t) {
        final double gamma_dpt;

        if (rh >= 1.0) {
            return t;
        }
        gamma_dpt = gamma(t) + Math.log(rh);
        return c * gamma_dpt / (b - gamma_dpt);
    }

    /**
     * Returns the Heat Index temperature.
     * The Heat Index is best designed for air temperature &ge; 27 &deg;C
     * and RH &ge; 40 %.
     *
     * @param t  the air temperature in degree Celsius.
     * @param rh  the relative humidity (%) in range of <code>0.0</code>
     *            to <code>1.0</code>.
     * @return the Heat Index temperature in degree Celsius.
     * @see #rhFromDewPoint(double, double)
     */
    public static double heatIndex(final double t, final double rh) {
        final double rh2 = rh * rh;
        final double t2 = t * t;
        final double hi;

        hi = - 0.03582 * t2 * rh2
                + 7.2546 * t * rh2
                - 164.24827778 * rh2
                + 0.2211732 * t2 * rh
                - 14.611605 * t * rh
                + 233.8548838888 * rh
                - 0.012308094 * t2
                + 1.61139411 * t
                - 8.784694755556;
        return (hi > t ? hi : t);
    }

    // /** Calculates the wind chill in degree Celsius.
    //  * @param celsiusTemp  Air temperature in degree Celsius.
    //  * @param windSpeed  Wind speed in meters per second.
    //  * @return  Wind chill in degree Celsius.
    //  */
    // public static short windChill(short celsiusTemp, short windSpeed) {
    //     return 0;
    // }
}
