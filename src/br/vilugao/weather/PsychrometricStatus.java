package br.vilugao.weather;

public final class PsychrometricStatus {
    private PsychrometricStatus() {}

    private static int toFahrenheit(final int c) {
        return (18 * c + 325) / 10;
    }

    public static final String[] DEWPOINT_STATUS_STRINGS = {
        /* <http://www.shorstmeyer.com/wxfaqs/humidity/humidity.html> */
        "A bit dry for some",
        "Very comfortable",
        "Comfortable",
        "OK for most",
        "Somewhat uncomfortable",
        "Quite uncomfortable",
        "Extremely uncomfortable",
        "Severely high",
    };

    public static int getDewPointStatusIndex(final int dpt) {
        /* <http://www.shorstmeyer.com/wxfaqs/humidity/humidity.html> */
        /* n = (round(F(dpt)) - 50) / 5 + 1; */
        final int n = (toFahrenheit(dpt) - 45) / 5;
        return (n < 0 ? 0 : n > 7 ? 7 : n);
    }

    public static final String[] RH_STATUS_STRINGS = {
        "OK",
        /* OMS's parameter */
        "I. Caution state",
        "II. Alert state",
        "III. Emergency state",
        /* My */
        "Humid",
        "Uncomfortably dry",
    };

    public static int getRHStatusIndex(final int rh) {
        return rh >= 75 ? 4
             : rh >= 40 ? 0
             : rh >= 30 ? 5
            /* OMS's parameter */
             : rh >= 20 ? 1
             : rh >= 12 ? 2
             : 3;
    }

    public static final String[] HEATINDEX_STATUS_STRINGS = {
        /* <http://www.nws.noaa.gov/os/heat/heat_index.shtml> */
        "OK",
        "I. Caution",
        "II. Extreme caution",
        "III. Danger",
        "IV. Extreme danger",
    };

    public static int getHeatIndexStatusIndex(final int hi) {
        /* <http://www.nws.noaa.gov/os/heat/heat_index.shtml> */
        final int f = toFahrenheit(hi);
        return f < 80 ? 0
             : f < 90 ? 1
             : f < 105 ? 2
             : f < 130 ? 3
             : 4;
    }
}
