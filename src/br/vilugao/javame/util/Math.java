package br.vilugao.javame.util;

public final class Math {
    private Math() {}

    /**
     * Returns the <code>double</code> value that is closest in value
     * to the argument and is equal to a mathematical integer. If two
     * <code>double</code> values that are mathematical integers are
     * equally close, the result is the integer value that is even.
     *
     * @param x  a <code>double</code> value.
     * @return   the closest floating-point value to <code>a</code>
     *           that is equal to a mathematical integer.
     */
    public static double rint(final double x) {
        final double twoToThe52 = 1L << 52;

        double xabs = java.lang.Math.abs(x);
        if (xabs < twoToThe52) {
            xabs = (twoToThe52 + xabs) - twoToThe52;
        }

        return (x > 0 ? 1.0 : -1.0) * xabs;
    }

    /**
     * Returns the natural logarithm (base <i>e</i>) of a <code>double</code>
     * value.
     *
     * @param x  a value.
     * @return   the natural logarithm of <code>x</code>.
     */
    public static double log(double x) {
        double ant = 0.0;
        double xp = --x;
        double res = xp;
        byte i = 2;

        /* ln(x) ~= (x-1) - sum((1-x)^i / i, i, 2, n). (Mercator series) */
        do {
            if (java.lang.Math.abs(res - ant) <= 1/4096.0) {
                break;
            }
            ant = res;
            xp *= -x;
            res += xp / i;
        } while (++i <= 32);

        return res;
    }


    /**
     * Returns Euler's number <i>e</i> raised to the power of a
     * <code>double</code> value.
     *
     * @param x  the exponent to raise <i>e</i> to.
     * @return   the value <i>e</i><sup><code>a</code></sup>,
     *           where <i>e</i> is the base of the natural logarithms.
     */
    public static double exp(double x) {
        double xp = x * x;
        double res = 0.0;

        /* exp(x) ~= 1 + x + sum(x^i * n! / i!, i, 2, n) / n!.
         * (Maclaurin series factored, with n = 7) */
        res += xp * 2520;
        xp *= x;
        res += xp * 840;
        xp *= x;
        res += xp * 210;
        xp *= x;
        res += xp * 42;
        xp *= x;
        res += xp * 7;
        xp *= x;

        return (res + xp) / 5040 + (x + 1.0);
    }
}
